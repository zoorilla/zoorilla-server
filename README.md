Zoorilla server
===============

### Maintainer
* Peter Cipov

### Authors
* Anton Dekterov
* Bohdan Mixanek
* Peter Cipov
* Zdenek Bzoch

### Source
* GitHub <https://github.com/zoorilla/zoorilla-server>


Documentation
-------------

### Requirements

* Java JDK 7
* Maven 3.0.5

### Build


```
mvn clean package
```

### How to run?

#### Production Environment
create zoorilla.json in same folder with backed binaries
```
{
    "zookeeper": {
        "host": "zoo.server.host",
        "port": 2181,
    }
}
```
mvn -Dcz.hack.config="path to zoorilla.json" exec:java

#### Develompment Environment
```
{
    "zookeeper": {
        "host": "localhost",
        "port": 2181,
        "type": "test",

        "data": [
            {
                "key" : "/a/b/c",
                "value" : "xxx",
                "type"  : "PERSISTENT"
            }
        ]
    }
}
```
mvn -Dcz.hack.config="path to zoorilla.json" exec:java
