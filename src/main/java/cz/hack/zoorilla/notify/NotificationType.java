
package cz.hack.zoorilla.notify;

/**
 *
 * @author pcipov
 */
public enum NotificationType {
	
	DATA,
	CHILDREN;
	
}
